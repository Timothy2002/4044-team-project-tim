# Flask MySQL resource

This demo application is featured in the Flask-MySQL workshop.

## Installation instructions

+ First clone the Team Project repository:

		git clone git@gitlab.com:LTUcompsci/4044-team-project.git

+ Rename `config.py.example` `config.py`:

		mv config.py.example config.py

+ Check Docker is running on your machine

+ Run:

		docker-compose up --build

You may need to wait a few minutes for the database to finish initialising.

+ Visit the demo app in a browser:

[http://0.0.0.0:5000](http://0.0.0.0:5000)
