from flask import render_template, jsonify
from app import app, mysql

# helper function
def get_rows(cursor):
    """ gets query results from cursor as
        list of dicts
    """
    fields = [i[0] for i in cursor.description]
    return [ dict(zip(fields, row)) for row in list(cursor.fetchall())]

@app.route('/')
def home():
    #spl=""
    #cursor = mysql.get_db().cursor()
    #cursor.execute ( sql )
    return render_template('home.html', page='Home')

if __name__ == '__main__':
  app.run(debug=True)
