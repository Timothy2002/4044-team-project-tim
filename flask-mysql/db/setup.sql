DROP DATABASE bikeapp_db;
CREATE DATABASE bikeapp_db;
USE bikeapp_db;
DROP TABLE IF EXISTS User, Bike, PickupPoint;

CREATE TABLE User (
	id INT AUTO_INCREMENT,
	email VARCHAR(45) UNIQUE NOT NULL,
	username VARCHAR(15) UNIQUE NOT NULL,
    is_admin BOOLEAN DEFAULT 0,
	password VARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;

INSERT INTO User Values (null, "bob@bob.com", "bobby123", 0, "password123");

CREATE TABLE PickupPoint (
	id INT AUTO_INCREMENT,
	postcode VARCHAR(10)
	PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE bike (
	id INT AUTO_INCREMENT,
	curr_loc INT, 
	model VARCHAR(10)
	PRIMARY KEY id,
	FOREIGN KEY curr_loc REFERENCES PickupPoint.id
)

INSERT INTO PickupPoint VALUES (null, "LS185HD");
INSERT INTO PickupPoint VALUES (null, "LS184HQ");

INSERT INTO Bike VALUES (null, "Trek", 1);
INSERT INTO Bike VALUES (null, "Trek", 2);
INSERT INTO Bike VALUES (null, "Trek", 3);
