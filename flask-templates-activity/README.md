# Flask templates resource

This code resource accompanies the Flask Templates video on Moodle in which Sorrel is demonstrating basic implementation of Jinga2 templates in a Flask application.

The application used in the demonstration is a fictitious 'LTU Digz' application which renders a list of rooms to rent.

To run this version of the application, clone the repository and run `python routes.py` from inside the app directory.
